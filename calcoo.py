import sys



class Calculadora:

	def __init__(self,operando1,operando2):

		self.operando1 = operando1

		self.operando2 = operando2

	def sumar(self):

		return self.operando1 + self.operando2

	def restar(self):

		return self.operando1 - self.operando2

#creamos el objeto micalcu del tipo Calculadora



if __name__ == "__main__":
	try:
		operando1 = float(sys.argv[1])
		operando2 = float(sys.argv[3])
		micalcu = Calculadora(operando1,operando2)
	except ValueError:
		sys.exit("Error: Non numerical parameters")


	if sys.argv[2] == "sumar":
		resultado = micalcu.sumar()
	elif sys.argv[2] == "restar":
		resultado = micalcu.restar()
	elif sys.argv[2] == "salir":
		sys.exit("Operacion no soportada")

	print(resultado)

